package
{
	import assetfy.Assetfy;
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureAtlas;
	
	/**
	 * ...
	 * @author Abraham Vazquez
	 */
	
	public class MyScene extends Sprite
	{
		
		public function MyScene()
		
		{
			super();
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			//draw the screen
			var bubbleAnim:bubble_anim = new bubble_anim();
			
			
			//THERES NO PIXELATION BECAUSE IS A VECTOR
			bubbleAnim.scaleX = 4;
			bubbleAnim.scaleY = 4;
			
			
			var ta:TextureAtlas = Assetfy.me(bubbleAnim, "texture_atlas"); // return starling TextureAtlas
			var starlingMc:MovieClip = new MovieClip(ta.getTextures('default'), Starling.current.nativeStage.frameRate); // Starling MovieClip
			
			starlingMc.x = 300;
			starlingMc.y = 300;
			
			//starlingMc.scaleX = 2;
			//starlingMc.scaleY = 2;
			
			Starling.juggler.add(starlingMc);
			
			addChild(starlingMc);
		
		}
		
		public function disposeTemporarily():void
		{
			//when instanced, dispose it, so it's invisible
			//this.visible = false;
			//remove listeners
		}
		
		public function initialize():void
		{
			//when instanced, initialize so it's visible
			//this.visible = true;			
			//add listenes
		}
	
	}

}