package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import starling.core.Starling;
	
	
	/**
	 * ... 
	 * @author Abraham Vazquez
	 */
	
	[SWF(width = "900", height = "600", frameRate = "60")]
	
	public class Main extends Sprite	
	{
		private var _starling:Starling;
		
		public function Main() 		
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			trace("USING -swf-version=32 AS COMPILER ARGUMENTS");
			_starling = new Starling(MyScene, stage, null, null, "auto", "baseline");
			
			
			_starling.antiAliasing = 1;
			_starling.start();
			
			
			
			
			
		}	
	
	}

}