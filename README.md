### What is this about? ###

Using [https://github.com/renanvaz/assetfy](Assetify) to display Flash (vector) movieclips in Starling.
Benefits like no pixelation when scaling (by scale the vector movieclip before converting to Satrling MovieClip).



### Some info ###

* Using a modified verison of Assetify since it was using Image.fromBitmap, I changed it to Texture.fromBitmap.
* using AIR 21 (with Flex SDK 4.15)
* swf-version=32 as compiler argument



### Who do I talk to? ###

* same username on Twitter